package lectures.part1basics

import scala.annotation.tailrec

object Recursion extends App {

  @tailrec
  def factorial(x: Int, acc: BigInt = 1): BigInt = x match {
    case y: Int if y <= 1 => acc
    case _ => factorial(x - 1, x * acc)
  }

  /*
  * Tasks:
  * 1. Concat a string n times
  * 2. IsPrime function tail recursion
  * 3. Fibonacci function tail recursive
  * */

  def concString(n: Int, string: String, acc: String = ""): String = n match {
    case x: Int if x < 1 => acc
    case _ => concString(n - 1, string, s"$acc $string")
  }

  println(concString(10, "hey"))

  def isPrime(n: Int, t: Int = 0, bool: Boolean = false): Boolean = bool match {
    case true => !bool
    case _: Boolean if t == 0 => isPrime(n, n - 1)
    case _: Boolean if t < 2 => true
    case _ => isPrime(n, t - 1, n % t == 0)
  }

  println(isPrime(5))


}
