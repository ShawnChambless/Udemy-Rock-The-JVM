package lectures.part1basics

object Functions extends App {
  def concat2(a: String, b: String): String = s"$a $b"

  def parameterless(): Int = 42

  parameterless() //both work
  parameterless //both work

  def looper(a: String, n: Int): String = {
    n match {
      case int: Int if int == 1 => a
      case _ => a + looper(a, n - 1)
    }
  }

  /*
  * Tasks:
  * 1. Greeting function (name, age)=> "Hi, my name is $name and I am $age years old
  * 2. Factorial function => 1 * 2 * 3 * ... * n
  * 3. Fibonacci function => 1, 1, 2, etc. f(n) = f(n - 1) + f(n - 2)
  * 4. Tests if a number is prime
  * */

  def greeting(name: String, age: Int) = s"Hi, my name is $name and I am $age years old"

  def factorial(n: Int): Int = {
    if (n <= 0) 1
    else n * factorial(n - 1)
  }

  def fib(n: Int, list: Vector[Int] = Vector.empty[Int]): Vector[Int] = n match {
    case 0 => list
    case _: Int if list.length < 2 => fib(n - 1, list :+ 1)
    case _ =>
      val last = list.last
      val secondToLast = list(list.length - 2)
      val ans = last + secondToLast
      fib(n - 1, list :+ (if(ans > 3) ans else last + 1))
  }

  println(factorial(5))
  println(fib(1))
}