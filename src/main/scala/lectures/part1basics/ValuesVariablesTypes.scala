package lectures.part1basics

object ValuesVariablesTypes extends App {
  val int: Int = 32
  val string: String = "slkdfj"
  val bool: Boolean = true
  val short: Short = 4221
  val long: Long = 92094820948209482L
  val float: Float = 2.0f
  val double: Double = 3.14
}
