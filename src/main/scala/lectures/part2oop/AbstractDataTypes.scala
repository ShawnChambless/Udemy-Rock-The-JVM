package lectures.part2oop

object AbstractDataTypes extends App {

  abstract class Animal {
    val creatureType: String

    def eat(): Unit
  }

  case class Dog(creatureType: String = "canine") extends Animal {
    override def eat(): Unit = println("crunch crunch")
  }

  case class Crocodile() extends Animal with Carnivore {
    override val creatureType: String = "Scary"

    def eat(): Unit = println("CHOMP")

    override def eat(animal: Animal = Dog()): Unit = println(s"I'm a $creatureType and I'm eating ${animal.creatureType}")
  }

  trait Carnivore {
    def eat(animal: Animal): Unit
  }

}
