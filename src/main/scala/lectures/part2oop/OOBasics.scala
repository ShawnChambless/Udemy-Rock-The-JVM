package lectures.part2oop

object OOBasics extends App {


  val person = Person("Angelo", 44)
  println(person.greet("Shawn"))

  val author = Writer("Shawn", "Chambless", 1988)
  val novel = Novel("Great Expectations", 1861, author)

  val newNovel = novel.copy(1904)

  println(novel)
  println(newNovel)

  val counter = Counter(10)
  println(counter.currentCount)
  println(counter.dec)
  println(counter.inc)
  println(counter.inc(5))
}

case class Person(name: String, age: Float) {
  def greet(name: String): Unit = println(s"${this.name} says hi $name")
}

/*
* Novel and a Writer
* Writer: first name, surname, year
*  - method fullname
*
*  Novel: age, year of release, author
*  - authorAge
*  - isWrittenBy(author)
*  - copy (new year of release) = new instance of Novel*/

case class Novel(name: String, releaseYear: Int, author: Writer) {
  def isWrittenBy: String = s"$name is written by ${author.fullName}"

  def copy(newYear: Int): Novel = Novel(name, newYear, author)
}

case class Writer(firstName: String, surname: String, year: Int) {
  def fullName: String = s"$firstName $surname"
}

/*
* Counter class
* - receives an int value
* - method current count
* - method to increment/decrement => new Counter
* - overload inc/dec to recieve an amount
* */

case class Counter(n: Int) {
  var count: Int = n

  def currentCount: Int = count

  def inc: Counter = Counter(count + 1)

  def inc(num: Int): Counter = Counter(count + num)

  def dec: Counter = Counter(count - 1)

  def dec(num: Int): Counter = Counter(count - num)

}

