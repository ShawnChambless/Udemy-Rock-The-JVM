package lectures.part2oop

object MethodNotations extends App {

  case class Person(name: String, favoriteMovie: String, age: Int = 0) {
    def likes(movie: String): Boolean = movie == favoriteMovie

    def hangOutWith(person: Person): String = s"$name is hanging out with ${person.name}"

    def unary_! : String = s"My name is $name!"

    def isAlive: Boolean = true

    def apply(): String = s"Hi my name is $name and I like $favoriteMovie"

    def +(nickname: String): String = s"$name (the $nickname)"

    def unary_+ : Person = Person(name, favoriteMovie, age = age + 1)

    def learns(skill: String): String = s"$name learns $skill"

    def learnsScala: String = learns("Scala")

    def apply(num: Int = 0): String = s"$name watched $favoriteMovie $num times"
  }

  val shawn = Person("Shawn", "Friday")
  // infix notation
  println(shawn likes "Inception")

  // "operators"
  val angelo = Person("Angelo", "Mean Girls")
  println(shawn hangOutWith angelo)

  // prefix notation
  val x: Boolean = -1 == 1.unary_-

  // unary_ prefix only works with a few operators => - + ~ !
  println(!shawn)

  // postfix notation
  println(shawn isAlive)

  // apply
  println(shawn.apply())
  println(shawn())

  /*
  * 1. Overload the + operator
  *   - mary + "the rockstar" => new person "Mary (the rockstar)"
  * 2. Add an age to the Person class
  *    Add a unary + operator => new person with age + 1
  * 3. Add a "learns" method in Person class. Receives String returns String "Person.name learns $string"
  *    Add a learnsScala method, calls learns method with "Scala" in postfix notation
  * 4. Overload apply to receive int and return string => "mary watched her favorite movie Inception 2 times"*/

  val nickName: String = shawn + "rockstar"
  val newAge: Person = +shawn
  val learningStuff: String = shawn learns "Stuff"
  val learningScala: String = shawn learnsScala

  println(s"$nickName\n$newAge\n$learningStuff\n$learningScala")
}
